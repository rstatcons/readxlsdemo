#' Generates reports using createDummyReport() and 
#' compresses them in zip-archive
#' 
#' @export
#' @examples 
#'  create_reports_archive("~/reports.zip", letters[12:14], new_interval(ymd(20141001), ymd(20150501)))

create_reports_archive <- function(archfile, ...) {
  
  inputdir <- tempfile("readxlsdemo")
  
  dir.create(inputdir)
  
  fileslist <- createDummyReport(..., outputDir = inputdir)
  
  fileslist <- file.path(inputdir, fileslist)
  
  zip(zipfile = archfile, 
      files = fileslist,
      extras = "--junk-path" # Store without full path
      )
  
  unlink(inputdir, recursive = TRUE)
  
  invisible(TRUE)
}
