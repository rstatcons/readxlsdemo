extract_form <- function(filename) {
  form <- stringr::str_extract(filename, 
                       "^[\\p{Alphabetic}\\p{Decimal_Number}]*")
  tolower(form)
}

extract_bank <- function(filename) {
  bank <- stringr::str_replace_all(filename, "^.*?_|_.*?$", "")
  tolower(bank)
}

extract_date <- function(filename) {
  date <- stringr::str_replace_all(filename, "^.*_|\\.xls*.$", "")
  date <- lubridate::dmy(paste0("01", date))
  if(any(is.na(date))) stop(paste0("Month and year are not recognized in ",
                              filename[is.na(date)]))
  date
}

extract_filetype <- function(filename) {
  type <- stringr::str_replace_all(filename, "^.*\\.", "")
  tolower(type)
}
