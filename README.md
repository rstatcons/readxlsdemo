# README

## Introduction

The package demonstrates the process of extraction of numbers from specific cells of spreadsheet files. Microsoft Excel files have a specfic unique structure. 

The file structure changes depending on the date on which the file was produced: in different historical time periods are associated with different, but valid, Excel template formats. Therefore the specific cells of interest move to differnt locations within the template.

## Basic usage

### Installation

```
devtools::install_git('https://username:password@gitlab.com/rstatcons/readxlsdemo.git')

```

Replace `username` and `password` with the credentials you have been supplied with.
### Example reports

Currently we don't have original input spreadsheet files, therefore we have developed an R function that generates example spreadsheet reports with fictitious numbers.

```
library(lubridate)
library(readxlsdemo)

create_reports_archive(
  archfile = "~/reports.zip", 
  bank = LETTERS, 
  when = new_interval(ymd(20141101), 
                      ymd(20150901)))

```

This chunk of R code will create an archive called 'reports.zip', which will be located in the home folder. The archive will contain monthly reports for banks (all letters of alphabet) for every month between November 2014 and September 2015. In this period we have two versions of forms (msfo2 and msfo3).


### Extraction of data

```
extract_data_from_file("~/reports.zip")
```

### How it works

All processes are currently governed by datasets contained within this package.

## TODO

### Download a file in Shiny

http://shiny.rstudio.com/articles/download.html

### Edit table online

https://cran.r-project.org/web/packages/rhandsontable/index.html